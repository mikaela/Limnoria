---
layout: default
---

Welcome to Mikaela's Supybot pages.

This site isn't official and won't help with most of issues. In case you
are looking for official sites, they are here:

* [Supybook](http://supybook.fealdia.org/devel/)
* [Supybot Website](http://supybot.aperio.fr/)
    * [Limnoria official documentation](http://doc.supybot.aperio.fr/)
* [Gribble Wiki](http://sourceforge.net/apps/mediawiki/gribble/index.php?title=Main_Page)

If you cannot find what you are looking for from them, please come to IRC 
and ask. The Support channels are
[#supybot,#limnoria on chat.freenode.net](ircs://chat.freenode.net:6697/%23supybot%2c%23limnoria)
