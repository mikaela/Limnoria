# Mikaela's fork of Limnoria.

There are mainly two branches. This which you are looking at, gh-pages
which is the source of <http://supybot.mikaela.info/>.

**testing** which will be synced with [ProgVal/Limnoria] when needed. It
is used as base for my changes which will be pull requested.

[ProgVal/Limnoria]:https://github.com/ProgVal/Limnoria.git
